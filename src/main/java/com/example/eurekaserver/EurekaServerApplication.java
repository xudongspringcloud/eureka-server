package com.example.eurekaserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;


@SpringBootApplication
@EnableEurekaServer
public class EurekaServerApplication {

    public static void main(String[] args) {
       // SpringApplication.run(EurekaServerApplication.class, args);
        //高版本的不行，要用其他的方法启动
         new SpringApplicationBuilder(EurekaServerApplication.class).web(true).run(args);
    }

}
